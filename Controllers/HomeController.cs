﻿using LVL2_ASPNet_MVC_02_JEFRYKURNIAWAN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace LVL2_ASPNet_MVC_02_JEFRYKURNIAWAN.Controllers
{
    public class HomeController : Controller
    {
        private DB_CodingIDEntities db = new DB_CodingIDEntities();
        public ActionResult Index()
        {
            return View(db.ms_employee.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ms_employee objEmployee) {
            using (DbContextTransaction transaction = db.Database.BeginTransaction()) {
                try
                {
                    if (ModelState.IsValid) {
                        db.ms_employee.Add(objEmployee);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    return View(objEmployee);
                }
                catch (Exception ex){
                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return View();
                }
            }
        }

        public ActionResult Update(int id) {
            
            return View(db.ms_employee.Where(x => x.pk_ms_employee_id == id).ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ms_employee objEmployee) {
            using (DbContextTransaction transaction = db.Database.BeginTransaction()) {
                try {
                    if (ModelState.IsValid) {
                        db.Entry(objEmployee).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    return View(objEmployee);
                }
                catch (Exception ex) {
                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return View();
                }
            }
        }

        public ActionResult Delete(int id) {
            using (DbContextTransaction transaction = db.Database.BeginTransaction()) {
                try {
                    if (ModelState.IsValid) {
                        ms_employee objEmployee = db.ms_employee.Where(x => x.pk_ms_employee_id == id).FirstOrDefault();
                        db.ms_employee.Remove(objEmployee);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index","Home");
                    }
                    return View();
                }
                catch (Exception ex) {
                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return View();
                }
            }
        }       
    }
}