﻿using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_02_JEFRYKURNIAWAN
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
