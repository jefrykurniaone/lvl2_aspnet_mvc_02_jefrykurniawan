﻿using System.Web;
using System.Web.Optimization;

namespace LVL2_ASPNet_MVC_02_JEFRYKURNIAWAN
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/assets/images/favicon.ico",
                "~/assets/css/bootstrap/css/bootstrap.min.css",
                "~/assets/icon/themify-icons/themify-icons.css",
                "~/assets/icon/font-awesome/css/font-awesome.min.css",
                "~/assets/icon/icofont/css/icofont.css",
                "~/assets/css/style.css",
                "~/assets/css/jquery.mCustomScrollbar.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                "~/assets/js/jquery/jquery.min.js",
                "~/assets/js/jquery-ui/jquery-ui.min.js",
                "~/assets/js/popper.js/popper.min.js",
                "~/assets/js/bootstrap/js/bootstrap.min.js",
                "~/assets/js/jquery-slimscroll/jquery.slimscroll.js",
                "~/assets/js/modernizr/modernizr.js",
                "~/assets/js/modernizr/css-scrollbars.js",
                "~/assets/js/script.js",
                "~/assets/js/pcoded.min.js",
                "~/assets/js/vartical-demo.js",
                "~/assets/js/jquery.mCustomScrollbar.concat.min.js"
                ));
        }
    }
}
